
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <title>Course</title>
</head>
<body>
    <section class="main-part">
        <header class="header">
            <ul class="header-list">
                <li class="header-item"><a href="reg.php" class="header-link">Создание Канала</a></li>
                <li class="header-item"><a href="list.php" class="header-link">Список Каналов</a></li>
                <li class="header-item"><a href="like.php" class="header-link">Доверенные Каналы</a></li>
            </ul>
        </header>

        <section class="list-channel">
            <div class="reg-line"></div>
            <div class="list-head">Список каналов</div>
                <?php
                    include 'view.php';
                    include 'swap.php';
                    echo getList();
                    
                ?>
        </section>
    </section>
</body>
</html>